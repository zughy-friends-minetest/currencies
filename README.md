# **API**
<br>

### `currencies.register(currency, def)`
Use this to register a currency, it is necessary to use every other function.
`def` is a table containing the following values (it isn't demanded): <br>
```lua
--[[
{
    min_value : number =
        the smallest balance that a player can have.

    max_value : number =
        the biggest balance that a player can have.
    
    negative_balance : bool =
        if the balance can be less than 0.
}
]]
```

<hr>

### `currencies.get(currency, pl_name)`
Use this to get a player's balance.

<hr>

### `currencies.set(currency, pl_name, value)`
Use this to set a player's balance.

<hr>

### `currencies.add(currency, pl_name, value)`
Use this to increase a player's balance.

<hr>

### `currencies.sub(currency, pl_name, value)`
Use this to decrease a player's balance.

<hr>

### `currencies.transfer(currency, from_pl_name, to_pl_name, amount)`
Use this to subtract `amount` money `from_pl_name` and add them to `to_pl_name`.

<hr>

### `currencies.exchange(currency, from_pl_name, from_amount, to_pl_name, to_amount)`
Use this to subtract `from_amount` from `from_pl_name` and add `to_amount` to `to_pl_name`.

<hr>

### `currencies.exists(currency)`
Use this to check if a currency has been registered.

<hr>

<br><br><br>

## Example
```lua
currencies.register("skywars_money", {min_value = 250})

currencies.add("skywars_money", "Giov4", 300)
currencies.sub("skywars_money", "Giov4", 100)

local balance = currencies.get("skywars_money", "Giov4")
print(balance)  -- output: 250
```
